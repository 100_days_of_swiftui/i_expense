//
//  Expenses.swift
//  iExpense
//
//  Created by Hariharan S on 18/05/24.
//

import Foundation
import Observation

@Observable
class Expenses {
    init() {
        if let savedItems = UserDefaults.standard.data(forKey: "ExpenseItems") {
            if let decodedItems = try? JSONDecoder().decode(
                [ExpenseItem].self,
                from: savedItems
            ) {
                self.items = decodedItems
                return
            }
        }
        self.items = []
    }
    
    var items = [ExpenseItem]() {
        didSet {
            if let encoded = try? JSONEncoder().encode(self.items) {
                UserDefaults.standard.set(encoded, forKey: "ExpenseItems")
            }
        }
    }
    
    var hasPersonalExpenses: Bool {
        var personal: Bool = false
        self.items.forEach { item in
            if item.type == "Personal" {
                personal = true
                return
            }
        }
        return personal
    }
    
    var hasBusinessExpenses: Bool {
        var business: Bool = false
        self.items.forEach { item in
            if item.type == "Business" {
                business = true
                return
            }
        }
        return business
    }
    
}
