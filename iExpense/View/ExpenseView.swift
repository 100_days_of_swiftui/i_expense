//
//  ExpenseView.swift
//  iExpense
//
//  Created by Hariharan S on 18/05/24.
//

import SwiftUI

struct ExpenseView: View {
    
    // MARK: - Properties
    
    @State private var expenses = Expenses()
    @State private var showingAddExpense = false
    
    var body: some View {
        NavigationStack {
            Form {
                if self.expenses.hasPersonalExpenses {
                    Section("Personal") {
                        List {
                            ForEach(self.expenses.items) { item in
                                if item.type == "Personal" {
                                    HStack {
                                        VStack(alignment: .leading) {
                                            Text(item.name)
                                                .font(.headline)
                                        }
                                        
                                        Spacer()
                                        
                                        Text(
                                            item.amount,
                                            format: .currency(code: item.currency)
                                        )
                                    }
                                }
                            }
                            .onDelete(perform: self.removeItems)
                        }
                    }
                }
                
                if self.expenses.hasBusinessExpenses {
                    Section("Business") {
                        List {
                            ForEach(self.expenses.items) { item in
                                if item.type == "Business" {
                                    HStack {
                                        VStack(alignment: .leading) {
                                            Text(item.name)
                                                .font(.headline)
                                        }
                                        
                                        Spacer()
                                        
                                        Text(
                                            item.amount,
                                            format: .currency(code: item.currency)
                                        )
                                    }
                                }
                            }
                            .onDelete(perform: self.removeItems)
                        }
                    }
                }
            }
            .navigationTitle("iExpense")
            .toolbar {
                Button(
                    "Add Expense",
                    systemImage: "plus"
                ) {
                    self.showingAddExpense = true
                }
            }
            .sheet(isPresented: self.$showingAddExpense) {
                AddView(expenses: self.expenses)
            }
        }
    }
}

// MARK: - Private Methods

private extension ExpenseView {
    func removeItems(at offsets: IndexSet) {
        self.expenses.items.remove(atOffsets: offsets)
    }
}

#Preview {
    ExpenseView()
}
