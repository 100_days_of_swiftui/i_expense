//
//  AddView.swift
//  iExpense
//
//  Created by Hariharan S on 18/05/24.
//

import SwiftUI

struct AddView: View {
    
    // MARK: - Environment Property

    @Environment(\.dismiss) var dismiss
    
    // MARK: - State Properties

    @State private var name = ""
    @State private var amount = 0.0
    @State private var type = "Personal"
    @State private var code = "INR"
    
    // MARK: - Properties

    var expenses: Expenses
    let types = ["Business", "Personal"]
    let currencyCodes = ["AED", "EUR", "INR", "JPY", "THB", "USD"]

    var body: some View {
        NavigationStack {
            Form {
                TextField(
                    "Name",
                    text: self.$name
                )

                Picker(
                    "Type",
                    selection: self.$type
                ) {
                    ForEach(
                        self.types,
                        id: \.self
                    ) {
                        Text($0)
                    }
                }
                
                Picker(
                    "Currency Code",
                    selection: self.$code
                ) {
                    ForEach(
                        self.currencyCodes,
                        id: \.self
                    ) {
                        Text($0)
                    }
                }

                TextField(
                    "Amount",
                    value: self.$amount,
                    format: .currency(code: self.code)
                )
                    .keyboardType(.decimalPad)
            }
            .navigationTitle(self.$name)
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                Button("Save") {
                    let item = ExpenseItem(
                        name: self.name,
                        type: self.type,
                        amount: self.amount, 
                        currency: self.code
                    )
                    self.expenses.items.append(item)
                    self.dismiss()
                }
            }
        }
    }
}

#Preview {
    AddView(expenses: Expenses())
}
