//
//  iExpenseApp.swift
//  iExpense
//
//  Created by Hariharan S on 18/05/24.
//

import SwiftUI

@main
struct iExpenseApp: App {
    var body: some Scene {
        WindowGroup {
            ExpenseView()
        }
    }
}
