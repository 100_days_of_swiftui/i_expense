//
//  ExpenseItem.swift
//  iExpense
//
//  Created by Hariharan S on 18/05/24.
//

import Foundation

struct ExpenseItem: Identifiable, Codable {
    var id = UUID()
    let name: String
    let type: String
    let amount: Double
    let currency: String
}
